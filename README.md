# Better Discord themes!
** **
**How do I use the Better Darkmode theme?**
** **
That can be done by going [here](https://gitlab.com/Iskawo/betterdiscord-themes/blob/master/Better%20Darkmode/BetterDarkmode.theme) then downloading.
** **
Once downloaded, go to **"C:\Users\{YourUser}\AppData\Roaming\BetterDiscord\themes"** then drop the "`BetterDarkmode.theme`" file in there and boom!